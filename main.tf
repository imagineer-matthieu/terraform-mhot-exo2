resource "scaleway_instance_server" "esgi_lille_server" {
  name        = "scw-terraform-${var.student_name}"
  image       = "ubuntu_focal"
  type        = var.instance_type
  tags        = ["terraform"]
  ip_id = scaleway_instance_ip.public_ip.id
}

resource "scaleway_instance_ip" "public_ip" {
  project_id = var.project
}
