variable "student_name" {
  type = string
}

variable "instance_type" {
  type    = string
  default = "DEV1-S"
}

variable "zone" {
  type = string
}

variable "project" {
  type    = string
  default = "b8330df7-e609-44bd-98ff-319b419e269c"
}
