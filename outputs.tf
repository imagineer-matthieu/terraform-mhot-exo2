output "public_ip" {
  value = scaleway_instance_server.esgi_lille_server.public_ip
}

output "ssh_command" {
  value = "ssh username@${scaleway_instance_server.esgi_lille_server.public_ip}"
}

output "nginx_url" {
  value = "http://${scaleway_instance_server.esgi_lille_server.public_ip}"
}